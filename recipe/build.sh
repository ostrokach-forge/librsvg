#! /bin/bash

set -ev

echo ${CPPFLAGS}
echo ${LDFLAGS}
echo ${CFLAGS}
echo ${CXXFLAGS}

which pkg-config

# # Linux-specific
# export CXXFLAGS="${CXXFLAGS} -DBOOST_MATH_DISABLE_FLOAT128"

# # Common
export CPPFLAGS="${CPPFLAGS} -I${PREFIX}/include -I${PREFIX}/include/cairo -I${PREFIX}/include/gdk-pixbuf-2.0 -I${PREFIX}/x86_64-conda_cos6-linux-gnu/sysroot/usr/include/gdk-pixbuf-2.0 -I${PREFIX}/../x86_64-conda_cos6-linux-gnu/sysroot/usr/include/gdk-pixbuf-2.0 -I${PREFIX}/../_build_env/x86_64-conda_cos6-linux-gnu/sysroot/usr/include/gdk-pixbuf-2.0 -I${PREFIX}/include/pycairo -I${PREFIX}/include/glib-2.0 -I${PREFIX}/include/gio-unix-2.0 -I${PREFIX}/lib/glib-2.0/include -I${PREFIX}/include/libxml2 -I${PREFIX}/include/pango-1.0 -I${PREFIX}/include/freetype2 -I${PREFIX}/include/libcroco-0.6"
# export LDFLAGS="${LDFLAGS} -L${PREFIX}/lib -L${PREFIX}/../_build_env/x86_64-conda_cos6-linux-gnu/sysroot/usr/lib -L${PREFIX}/../_build_env/x86_64-conda_cos6-linux-gnu/sysroot/usr/lib64"
# export CFLAGS="${CFLAGS} -m${ARCH}"
# export CXXFLAGS="${CXXFLAGS} -m${ARCH}"

export LIBRSVG_CFLAGS="${CFLAGS}"
export LIBRSVG_LIBS="${LDFLAGS}"

export GTHREAD_CFLAGS="${CFLAGS}"
export GTHREAD_LIBS="${LDFLAGS}"

export GMODULE_CFLAGS="${CFLAGS}"
export GMODULE_LIBS="${LDFLAGS}"

export RSVG_CONVERT_CFLAGS="${CFLAGS}"
export RSVG_CONVERT_LIBS="${LDFLAGS}"

export GDK_PIXBUF_CFLAGS="${CFLAGS}"
export GDK_PIXBUF_LIBS="${LDFLAGS}"

export PKG_CONFIG_PATH="${PKG_CONFIG_PATH:+${PKG_CONFIG_PATH}:}${PREFIX}/lib/pkgconfig:${PREFIX}/share/pkgconfig"

export PATH="${PATH}:$(dirname ${PREFIX})/_build_env/x86_64-conda_cos6-linux-gnu/sysroot/usr/bin"

which gdk-pixbuf-query-loaders-64

# Set CARGO linker and archiver
mkdir .cargo
cat <<EOF > .cargo/config
[target.x86_64-unknown-linux-gnu]
linker = "${CC}"
ar = "${AR}"
EOF

# Dependencies have corrupt libtool files, leading to error:
# `../lib/libiconv.la' is not a valid libtool archive`
rm -f $PREFIX/lib/*.la

configure_args=(
    --prefix=$PREFIX
    --disable-Bsymbolic
    --build=x86_64-unknown-linux-gnu
    --host=x86_64-unknown-linux-gnu
    --disable-static
    --disable-gtk-doc
)

# rm -f $PREFIX/lib/*.la  # deps have busted files
./configure "${configure_args[@]}" || { cat config.log ; exit 1 ; }
make -j$CPU_COUNT
make install

# cd $PREFIX
# find . '(' -name '*.la' -o -name '*.a' ')' -delete
rm -rf $PREFIX/share/gtk-doc

# ./configure --build=x86_64-redhat-linux-gnu --host=x86_64-redhat-linux-gnu --program-prefix= --disable-dependency-tracking --prefix=/usr --exec-prefix=/usr --bindir=/usr/bin --sbindir=/usr/sbin --sysconfdir=/etc --datadir=/usr/share --includedir=/usr/include --libdir=/usr/lib64 --libexecdir=/usr/libexec --localstatedir=/var --sharedstatedir=/var/lib --mandir=/usr/share/man --infodir=/usr/share/info --disable-static --disable-gtk-doc --enable-introspection --enable-vala
